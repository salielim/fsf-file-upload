## File Upload in MEAN stack app
This is a practice app during the NUS Full Stack Foundation course.

## How to get it up and running
* `bower install`
* `npm install`
* Start server with `node server/app.js` or `nodemon`
* Visit the localhost port indicated on your server.
