// Dependencies --------------------------------------------------------------------------------------------------------------
// Loads express module and assigns it to a var called express
var express = require("express");

// Loads path to access helper functions for working with files and directory paths
var path = require("path");

// Creates an instance of express called app
var app = express();


// Constants  --------------------------------------------------------------------------------------------------------------
// Defines server port.
// Value of NODE_PORT is taken from the user environment if defined; port 3000 is used otherwise.
const NODE_PORT = process.env.NODE_PORT || 8080;


// Paths --------------------------------------------------------------------------------------------------------------
// __dirname is a global that holds the directory name of the current module
const CLIENT_FOLDER = path.join(__dirname + '/../client');  // CLIENT FOLDER is the public directory
// TODO: 3.1  Assign path to the messages folder to the constant MSG_FOLDER
const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');


// Middlewares  --------------------------------------------------------------------------------------------------------------
app.use(express.static(CLIENT_FOLDER));

// Modularization of server files. Use require to import the files into app.js
require("./route")(app);


// Error Handling  --------------------------------------------------------------------------------------------------------------
// Handles 404. In Express, 404 responses are not the result of an error, 
// so the error-handler middleware will not capture them.
// To handle a 404 response, add a middleware function at the very bottom of the stack
// (below all other path handlers)
app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

// Error handler: server error
app.use(function (err, req, res, next) {
    res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
});


// Start server  --------------------------------------------------------------------------------------------------------------
// Server starts and listens on NODE_PORT
app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});